# Huitième leçon

## Des Questions
 * Êtes-vous étudiant?
   >Oui, Je suis étudiant

* Êtes-vous docteur? 
   >Non, Je ne suis pas docteur, Je suis étudiant

* Avez-vous parlé Portugais hier?
   >Oui, hier j'ai parlé portugais

* Avez-vous parlé Japonais hier?
   >Non, hien j'ne ai pas parlé Japonais , j'ai parlé Portugais
---
## Texte

Créer du texte avec ces mots

* Je suis 
* Je vais 
* Je parle 
* J'habite 
* J'comprends bien  
* J'aime 
* Je ne sais pas
* Je veux
* Je peux
* J'ai besoin de
* Il y a
* Mon
* Ma
* Mes
* il
* elle

---
## Fautes d'orthographe

Je vais à l'école de biciclete (**à vélo**) 

Je vais au travail de (**en**) autobus

Je n'ai frère (**Je n'ai pas de frère**)

Je n'ai voiture (**Je n'ai pas de voiture**)

---
Beaucap ---> quantidade

Trés ---> intensidade

Bon marché -  loin  -  pres