# Première leçon

## Nombres
        1. un/une
        2. deux
        3. trois
        4. quatre
        5. cinq
        6. six
        7. sept 
        8. huit
        9. neuf
        10. dix

---
## Citations
- J'habite à Brasilia
- Vous habitez à Rio
- Il habite à Paris
- Elle habite à São Paulo
- Je parle Portugais
- Vous parlez anglais
- Il parle Japonais
- Elle parle Italien
- Parlez-vous anglais?
  >- Oui, je parle anglais
  >- Non, je ne parle pas anglais
- Oú habitez-vous?
- Habitez-vous à Rio?
  >- Oui, J'habite à Rio
  >- Je n'habite pas à Rio, J'habite à São Paulo
- Je m'appelle Robert mon nom est Dubais et je suis Français, je suis ne à Lyon
- Oú êtes vous né?

---
## Mots

maison - homme - femme - professeur - étudiant/étudiante - secrétaire 

---
## Nationalité

mâle | femelle
----------|---------
Brésilien | bresiliénne
Anglais   | anglaise
Portugais | portugaíse
Japonais  | japonaise

---
## Verbe Etrê
        
Je **suis** professeur

Tu **es** étudiant

il **est** anglais

elle **est** française

Nous **sommes** à Lyon

Vous **être** brésiliens

ils **sont** ici


---
## Conjugasion du verbe

Pronom personnel | Parle   | Chanter | Travailler
-----------------|---------|---------|-----------
Je               | parle   | chante  | travaille      
Tu               | parles  | chantes | travailles
Il               | parle   | chante  | travaille
Elle             | parle   | chante  | travaille
Nous             | parlons | chantons| travaillons
Vous             | parlez  | chantez | travaillez
Ils              | parlent | chantent| travaillent
