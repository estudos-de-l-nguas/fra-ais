# Troisième leçon

Complétez avec: en, au:
* Je vais **au** théâter
* Tu vas **en** Juasse
* Elle va **au** Pérou et nous allons **en** cinéma

---

Complétez avec: vais, va, vas, allons:
    
* Il **va** au théater, elle **va** en France, tu **vas** au Japon et nous **allons** en France

--- 
Compétez:
* Je **suis** français
* Tu **es** anglais
* Il **est** Japonais
* Nous **sommes** bien
* Ils **sont** très mal

---

## Répondez

* Oú habitez-vous?
  > Je habitez à ...

* Parlez-vous Francais?
   > No, Je ne parle pas Français, Je parlez Portugais

* Parlez-vous Anglais? 
    > Oui, Je parlez Anglais

* Oú travaillez-vous?
   > Je ne travalle pas

* Travaillez-vous à Brasília?
   > Oui, Je travalle à Brasília

* Comment allez-vous à l'école?
   > Je vais à l'école en autobus

* Êtes-vous étudiant?
   > Oui, je suis etudiant

* Étes-vous professeur?
   > No, Je ne suis pas professeur, je suis etudiant

* Est Maria secrétaire?
    > Oui, Maria est secrétaire

* Est Luis chanteur?
    > Oui, Luiz est chanteur

---

## Completè avec: mon - ma - mes

**Ma** mere parle français et **mon** pére parle anglais

**Mon** frére habite en France et **ma** soeur habite au Canada

**Mes** amis parlent français.

J'ai deux livre de français. Robert a un livre de français. **Mes** livres sont ici, **mon** livre est à maison

---

il fait chaud

il fait froid

---

## Verbe avoir

Pronom | Avoir 
-------|-------
Je | ai
Tu | as
Il | a 
Elle | a
Nous | avons
Vous | avez
Ils | ont

### Exemple
Affirmatif | Negatif
-----------|----
J'**ai** une maison | Je **n'ai pas de** maison
Tu **as** une voiture | Tu **n'as pas de** voiture 
Il **a** un livre de français | Il **n'a pas un livre de** français
Elle **a** un montre | Elle **n'a pas de** montre
Nous **avons** des amis à Paris | Nous **n'avons pas des** amis à Paris
Vous **avez** des amis à Rio | Vous **n'avez pas des** amis à Rio
Ils **ont** des livre de anglais | Ils **n'ont pas des** livre d'anglais

---

## Citations

Maria a dix **élèves** à anglais

Elle n'a **pas de** élèves d'anglais

à Brasília **il y a** un aéroport

à Rio **il y a** deux aéroports

à Brasília **il n'y a pas** cinq aéroports

à Rio **il n'y a pas** dix aéroports

Je suis **beau**

Je suis **belle** / **jolie**

Il est **laid**

Elle est **laide**

---

## Des questions

Comment s'appelle votre mère?

Á quelle heure allez-vous au travail?

Comment allez-vous au travail?

Marco étudiez vous le français?

Avez-vous de frère et de soeur
 >Oui, j'ai un frère et un soeur

 >Non, je'n ai pas de frère et de soeur 

 À quelle heur vous vous couchez?

 Comment allez vous?




