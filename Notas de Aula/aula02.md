# Deuxième leçon

## Des questions
* Quel est votre nom?

  > Mon nom est ...
* Quel est votre pre nom?

  >Mon pre nom est ...
* Oú habitez-vous?
  >Je habitez à ...

* Oú travailler-vous?
  >Je'n travaille pas

* Habitez-vous en France?
  >Non, Je'n habite en France, Je habite à Bresilien

* Quel âge avez-vous?
  >J'ai soixante ans


---
## Nombres

        11 - onze
        12 - douze
        13 - treize
        14 - quatorze
        15 - quinze
        16 - seize
        17 - dix-sept
        18 - dix-huit
        19 - dix-neuf
        20 - vingt
        30 - trente
        40 - quarante
        50 - cinquante
        60 - soixante

---
## Conjugasion du verbe

Pronom | Marcher  | Allez
--------|---------|-------
Je      | marche  | vais
Tu      | marches | vas
Il      | marche  | va
Elle    | marche  | va
Nous    | marchons| allons
Vous    | marchez | allez
Ils     | marchent| vont

---
## Citations

 * Je vais à Brasilia
 * Tu vas au supermaché
 * Il va à Gama
 * Elle va à la plage
 * Nous allons à Paris
 * Vous allez au theâtre
 * Ils vont en Argentine
 ---

 * Vous allez à la plage
 * Allez-vous à la plage?
 >Oui, Je vais à la palge

 >Non, Je ne vais pas à la plage, Je vais ou cinéma

 --- 

## Afirmatif et négatif

Afirmatif | Négatif
----------|--------
Je suís professeur | Je ne suis pas professeur
Tu es mon ami | Tu n'es pas mon ami
Il est mon frére | Il n'est pas mon frére
Nous sommes ici | Nous ne sommes pas à la maison
Vous êtes à l'ecole | Vous n'êtes pas à l'ecole
Ils sont à Rio | Ils ne sont pas à Rio 