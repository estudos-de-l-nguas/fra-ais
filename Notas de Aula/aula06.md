# Sixième leçon

## Verbe Manger (indicatif présent)

Pronom | Manger
-------|------
Je | mange
Tu | manges 
Il/ Elle | mange
Nous | mangeons
Vous | mangez
Ils/Elles | mangent

## Conjugaison du verbe (Indicatif futur)

Pronom | Parler | Marcher | Chanter
-------|--------|---------|--------
Je | parlerais | marcherais | chanterais
Tu |  parleras | marcheras | chanteras
Il/ Elle | parlera | marchera | chantera
Nous | parlerons | marcherons | chantereons
Vous | parlerez | marcherez | chanterez
Ils/Elles | parleront | marcheront | chanteront

---

**Aujourd'hui** Je suis ici

**hier** J'étais à Luziânia

**demain** Je serais à João Pessoa

---

Verbe Être (Indicatif imparfait)

Pronom | Être
-------|------
Je | étais
Tu |  étais
Il/ Elle | était
Nous | étions
Vous | étiez
Ils/Elles | étaient

---
Aujord'hui Je parle portugais

Hier **J'ai parlé** portugais

hier tu **as parlé**  portugais

il **a parlé**

nous **avons parlé**

vous **avez parlé**

ils **ont parlé**

---

Vous avez parlé français hier

   >Vous avez parlé français hier?

   > Avez vous parlé français heir?

   >Est-ce que vous avez parlé français hien?
   
Vous avez acheté une voiture hier
   >Vous avez acheté une voiture hier?

   >Avez vous acheté une voiture hier?
   
   >Est-ce que vous avez acheté une voitura hier?

---
* Qué est-ce que c'est?

* C'est **un** stylo


Avez-vous parlé Portugais hier?
   > Oui, j'ai parlé portuguais hier

   > Non, Je n'ai pas parlé portugais hien, J'ai parlé l'anglais

Je travalle à Gamá

Je ne travalle à Gamá

J'ai travalle à Gamá

Je n'ai pas travalle à Gamá

---
## Ses- son - sa
J'ai un frère et a une voiture et un chien, **son** chien est ici et **sa** voiture est à la maison, mais **ses** amis sont à l'école.

---

## Mon - ma - mes

**Mes** frère habite à Lyon, **ma** soeur habite à Paris et **mon** oncle habite ici

---

## Notre - nos - votre - vos

Vous habitez ici, mais **votre** père oú habite-il?

Nous parlons bien, mais **nos** amis ne parlent pas bien

Oú sont **vos** amis?

Nous sommers ici, mais **notre** pére est à la l'école

---

## Le - La - Les

* **la** tomante
* **La** minute
* **la** mer
* **la** planete
* **la** vídeo
* **la** voiture
* **le** matin
* **le** garage
* **le** stylo
* **les** tomates
* **les** plantes 

---
**il y a**

**il faut**


Je vais à Brasilia

Allez-vous à Brasília?
   >Oui, Je vais à Brasília
   
   >Oui, Jy vais