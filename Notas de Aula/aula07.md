# Septiéme leçon

## Texte

créer du texte avec ces mots

suis - habite - parle - travalle - ai - aime - vais - ne pas

---

## Mon /Ma

C'est **mon** père

C'est **ma** mère

C'est **mon** chien

C'est **ma** voiture

C'est **mon** fils

C'est **ma** maison

C'est **mon** livre

C'est **ma** ville

C'est **ma** femme

C'est **mon** ami


----
## Fautes d'orthographe

Espanol (**Espagnol**)

Je vais en (**à l'**) ecole

Je vais parler anglais en (**aux**) USA

Ma (**mon**) père s'appelle Robert

Mon (**ma**) mère s'appelle Mario

J'habitez(**habite**) long (**lion**) de Rio

Paul es mon ami, il es(**est**) de professeur

Je vais au travail de bus (**autobus**)

Je vais à Rio de (**en**) avion

Je suis né pas (**né suis pas**) professeur

Je ne(**né**) parle pas Japonais

---
## La Musique

### Aline
J'avais dessiné sur le sable

Son doux visage qui me souriait,

Puis il a plu sur cette plage.

Dans cet orage elle a disparu.

Et j'ai crié, crié... "Aline!" pour qu'elle revienne.

Et j'ai pleuré, pleuré... Oh j'avais trop de peine.

Je me suis assis auprès de son âme

Mais la belle dame s'était enfuie.

Je l'ai cherchée sans plus y croire

Et sans un espoir pour me guider.

Et j'ai crié, crié... "Aline!" pour qu'elle revienne.

Et j'ai pleuré, pleuré... Oh j'avais trop de peine.

Je n'ai gardé que ce doux visage

Comme une épave sur le sable mouillé.

Et j'ai crié, crié... "Aline!" pour qu'elle revienne.

Et j'ai pleuré, pleuré... Oh j'avais trop de peine.

Et j'ai crié, crié... "Aline!" pour qu'elle revienne.

Et j'ai pleuré, pleuré... Oh j'avais trop de peine.

Et j'ai crié, crié... "Aline!" pour qu'elle revienne.

Et j'ai pleuré, pleuré... Oh j'avais trop de peine.
