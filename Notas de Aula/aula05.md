# Cinquième leçon

## Completez avec: ma, mon, mes

**Mon** père s'appelle Jean

**Ma** mère s'appelle Maria

**Mes** frère parlent français

---

## Completez avec: son, sa, ses, mon, ma, leur

Robert est **mon** ami, il a un livre de français **son** livre est à la maison

Maria est **ma** soeur, elle a un ami et une amie, **ses** amis parlent français

Pierre et Jean ont une voiture, **leur** voiture est nouvelle.

---

## Complétez avec: notre, nos, votre, vos
Nous avons deux frère, **nos** frères habitent à Paris.

Avez-vous  des frère, **vos** frère, oú Ils habitent?

**Notre** maison est très jolie. Mère, **votre** maison n'est pas


---

J'ai **une** maison à Gama

J'ai **une** amie à Gama, elle s'appelle Maria

J'ai **des** amis à Paris, ils ne parlent pas Portugais

Jai **un** chien, ils s'appelle Kira

J'ai **des** chiens à la maison, ils sont jolis

---

Je bois **du** café

Je bois **du** lait

Je bois **de l'** eua

Je bois **du** jus d'orange

Je monge **de la** banana

Je monge **du** pain

Je monge **du** poulent

Je monge **du** riz

Tu manges **de la** salade

Il monge **des** herecots

Vous monge **du** sucre

---

## Completez avec: ai, a, avez, ont

Mes amis, **ont** une nouvelle maison

J'**ai** deux amis à Rio

---

## Répondez avec questions:

João, parle-t-il Portugais?

>Oui, il parle Portugais

Renata, parle-t-elle anglais?

>Non, elle ne parle pas anglais

Oú  étudiez-vous?

>Je étudiez ici

Porquoi étudiez-vous le français?

>Je étudiez francais parce que ...

Á quelle heure allez-vous à l'école?

>Je vais à l'école à 7 heure

--- 

## Complétez:

Je **me** réveille

Tu **te** réveilles

Il **se** réveille

Nous **nous** réveillon

Vous **vous** réveiller

Ils **se** reveillent

---

Je n'appelle

Tu t'appelles

Il s'appelle

Nous nous appellons

Vous vous appellez

Ils s'appellent

---

## Verbe Pouvoir

Pronom | Pouvoir
-------|---
Je | peux
Tu | peux
Il | peut
Nous | pouvons
Vous | povez
Ils | pouvent