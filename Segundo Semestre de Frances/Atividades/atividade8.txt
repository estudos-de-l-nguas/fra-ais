Complétez le texte avec: j'ai, j'avais, j'aurai, a, avait, aura, j'aurai.
Il a deux maisons, mais quand il habitait à Fortaleza il avais seulement une maison, mais l'année prochaine il aura trois maisons.
Quand j'habitais à Teresina j'avais un chien, aujourd'hui j'ai deux chiens, mais l'année prochaine j'aurai cinq chiens.

Complétez le texte avec: il y a, il y avait, il y aura: 
Hier il y avait cinq élèves ici, demain il y aura vingt, mais aujourd'hui il y a seulement deux.
