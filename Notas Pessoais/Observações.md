# Observações 

Aqui estarei colocando coisas variadas, assuntos em que tive duvidas e estudei, detalhes, e qualquer tipo de estudo que possa agregar mais para meu conhecimento.

---

## Quando usar ou nao a preposição "à"

A preposição "à" é escrita com o acento grave para diferenciar do verbo "avoir" terceira pessoa do singular que é (Il a, Elle a)

Ela pode ser usada quando se estiver falando em localizaçao destino, distância, quando falar do horario que algo vai começar. Exemplo

> Je vais **à** São Paulo

>Je suis **à** Lyon

>La place est **à** 100 mètres d'ici

>La pièce commence **à** 8 heures

Porem quando estamos falando de um país algumas coisas mudam.

Se o nome do país for feminino (Paises terminados com "e") ou começado com vocal é usado a preposição "en". Exemplo:

>Je suis **en** France

>Je vais **en** Angleterre

Se o nome do país for masculino será utilizado a preposição "au":

>Je suis **au** Brésil

>Je suis **au** Portugal

Se o nome do país estiver no plural será utilizado a preposição no plural que é "aux":

>Je suis **aux** Étates-Unis

Tambem utilizamos quando queremos dizer de que algo é feito ou para que serve um objeto:

>Une tarte **au** chocolat

Tambem utilizamos quando queremos dizer para que serve um objeto:

>Une tasse **à** café

>Une tasse **à** thé

**OBS**: Quando queremos descrever o conteúdo que está detro do objeto, utilizamos a preposição "de":

>Une tasse **de** thé

Pode ser usada quando queremos falar o valor de um objeto:

>Une voiture à 20.000 euros

É utilizada tambem, quando utilizamos um meio de transporte que nao entramos dentro dele como por exemplo uma bicicleta, moto ou os prorios pés:

>Je vais **à** vélo

>Je vais **à** pied

>Je vais **à** moto

Quando estamos dentre do meio de tranporte,utilizamos "en":
>Je vais **en** train

>Je vais **en** taxi

>Je vais **en** métro

---

## Negação
Apesar de existir outras formas de negação em frances a mais comum é a "**ne ...  ... pas**". Exemplo:
>Je ne suis pas en vacances

>Le dimanche, les magasins ne sont pas ouverts

Quando a expressão dentro da negação começar com uma vogal, é feito a contraçao do **ne** juntando o a vogal. Exemplo:

>Il n'est pas fatigué

>Je n'ai pas faim

Porém existe outras formas de negação que estarei listado abaixo.

### Ne...  ... Jamais

"**Ne... ...Jamais**" é o mesmo que "Nunca" em portugues. Exemplo:

>Je ne bois jamais de café

**OBS**: Sempre depois da segunda palavra negativa(PAS, JAMAIS, RIEN) é usado o "de". 

>PAS/JAMAIS/RIEN ... + DE

### Ne ... ... Plus 
"Ne... ...Plus" é o mesmo que "nao.. ...mais" em português. Exemplo:

>Je n'habite plus à Rio

### Ne ... ... Pas Enconre
"Ne ... ... Pas Enconre" é o mesmo que "ainda nao":

>Tu es déjà allé à Paris?

>Non, pas encore

>Non, je ne suis pas encore allé à paris

### Ne... ...rien
Esse é o mesmo que "nada" em portugues:

* Tu  veux quelque chose pour manger?

   >Non merci, je ne veux rien

### Ne... ...Personne
É o mesmo que "ninguem"

* Vous attendez quelq'un?

  >Non, Je n'attends personne.

outro exemplo:

>Je ne connais personne

### Ni
O "ni" é o mesmo que "nem":

>Je ne parle ni italien ni grec

----
.

.

.



---

## Coisas que quero colocar aqui

* Paises 
* NUneros naturais e ordinários
* Dias da semana
* Meses
* numeros de 0 a 1000
* Palavras do Duolingo
* diferença entre à ou en
* quando usar o la ou nao exemplo: Elle va à la plage | Nous allons à Paris
* parentes
 *Expressoes
 *Quando usar o "de" e o "de"
 * Tempos verbais
 * Diferença entre 'beau' 'belle' 'jolie'
 * Adjetivos
 * Os pronomes possesivos em francês
 * Como fazer perguntas

* Dias da semana
* Plurais
* Écoutez la chanson: "Elle était si jolie"; chanteur: Alain Barrière.