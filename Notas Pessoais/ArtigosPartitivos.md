fonte : https://francesautentico.com.br/como-usar-du-de-la-de-l-e-des-em-frances/

Em francês temos um tipo de artigo que se chama partitivo. O artigo partitivo serve para indicar uma quantidade indeterminada (uns, alguns, um pouco de).

Os artigos partitivos em francês são:

du – masculino singular
de la – feminino singular
de l’ – masculino ou feminino antes de uma vogal ou h mudo
des – masculino/feminino plural

A forma do partitivo depende de três coisas: o gênero, o número e a primeira letra do substantivo.

– Se o substantivo for plural, use des.
– Se for singular começando com vogal ou h mudo, use de l’.
– Se for singular e começar com consoante ou h aspirado use du para masculino e de la para feminino.

 

Agora menos teoria e alguns exemplos:

J’adore boire du café.
Adoro tomar café.

As-tu bu du thé?
Você bebeu chá? (não sei a quantidade certa, seja uma xícara ou um litro)

J’ai mangé de la salade hier.
Comi salada ontem. (sem saber a quantidade)

Nous allons prendre de la glace.
Vamos tomar sorvete. (não sabemos se vão ser duas bolas, uma casquinha etc)

Je voudrais de l’eau, s’il vous plait.
Gostaria de um pouco de água, por favor. (Talvez um copo, talvez uma garrafa…)

Le professeur a de la patience.
O professor tem paciência. (Não sabemos se tem muita ou pouca, mas sabemos que tem.)

Voici du gâteau.
Aqui está o bolo. (uma parte dele, não todo o bolo)

J’ai des Euros.
Tenho uns euros. (mais que um, mas não vou te dizer exatamente quantos)

Je vais acheter des pommes.
Vou comprar umas maçãs. (Não sei quantas, mas vai ser mais que uma.)

Elle a des amis formidables.
Ela tem uns amigos formidáveis.

Já pegou seu e-book com 150 expressões francesas com áudio (e grátis!). Clique aqui!

Agora atenção: depois de advérbios de quantidade, use somente de.

Il y a beaucoup de thé.
Tem muito chá.

J’ai moins de glace que Thierry.
Tenho menos sorvete que o Thierry.

 

Algumas expressões de quantidade:

un verre de vin – um copo de vinho
une bouteille de champagne – uma garrafa de champagne
une carafe d’eau – uma jarra d’agua
un litre de jus de pomme – um litro de suco de maçã
une assiette de charcuterie – um prato de frios
un kilo de pommes de terre – um quilo de batatas
une botte de carottes – um maço de cenouras
une barquette de fraises – uma caixa de morangos
une part de tarte – uma fatia de torta
un peu de fromage – um pouco de queijo
beaucoup de lait – muito leite
quelques morceaux de lards – alguns pedaços de bacon

 

Na negativa, o artigo partitivo é sempre de.

J’ai mangé de la soupe. – Eu tomei sopa.
Je n’ai pas mangé de soupe. – Eu não tomei sopa.

J’ai des amis. – Tenho uns amigos.
Je n’ai pas d’amis. – Não tenho amigos.

Elle a beaucoup de patience. – Ela tem muita paciência.
Elle n’a pas de patience. – Ela não tem paciência.

Il y a 5 livres. – Ele tem 5 livros.
Il n’y a pas de livre. – Ele não tem (nenhum) livro.